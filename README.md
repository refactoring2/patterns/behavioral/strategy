<h2>Strategy pattern</h2>
<h5>
&zwnj;&zwnj; &zwnj; &zwnj;Strategy is a behavioral design pattern, which define a family of similar algorithms
that divided by classes and can be replaced each other at runtime.
</h5>
<hr>

<h5>&zwnj;&zwnj; &zwnj; &zwnj; A predator on his new bike went to a jungle to the spacecraft. 
At the same time a squad of commandos, led by Dutch, landed in the jungle to destroy guerrillas and free hostages.
Dutch's team reached the enemy camp and killed all the guerrillas. The predator saw how the commandos killed people. 
He decided that there was no honor, and the team should be died. He killed the whole team except one... 
<br>
You, as the predator, must finish him.</h5>
<h4>Good luck! </h4>

<img src="img/predator.gif" alt="Predator rides on motorcycle!" width="480" height="270">

<hr>

<h5>&zwnj;&zwnj; &zwnj; &zwnj;The main feature of the predator is a hunt. We can describe hunting algorithm in the predator class, and it will be fine if algorithm is only one. 
Let's remember the movie, each teammate, except one, was annihilated by different ways, this mens - different hunting algorithms for the predator.
If we each time will be expanded predator class by hunting algorithms it will be extremely long. 
<img src="img/predator-impl-in.jpg">
<br>
In that case we can use the strategy design pattern, and describe each hunting algorithms in a separate class.
A hunting strategy will be chosen at runtime by the hunter depending on the name of enemy, and will be returned to a front.
</h5>
<img src="img/game.png" width="650" height="450">
<h5>
&zwnj;&zwnj; &zwnj; &zwnj;In start of the app an enemy name will be shown for us. We click on a button with the enemy name. 
In this case the PredatorController handle a request with the enemy name, and send it further to the HuntStrategyFactory.
Here in the HuntStrategyFactory existed a beans with realization of strategies. 
The HuntStrategyFactory will select one from existed beans based on the enemy name, and return to the PredatorController as a response.
Further the PredatorController set up the specific strategy bean to the PredatorService and call method doHunt() in the PredatorService. 
The PredatorService just call method doHunt() in the concrete realization of the HuntingStrategy, which executes the method.
The PredatorService simply calls the doHunt () method on the concrete HuntingStrategy implementation that executes the method.
</h5>
<img src="img/predator-schema.png" alt="Predator schema." width="511" height="493">