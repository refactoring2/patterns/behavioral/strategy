$(document).ready(function () {

    $('#data').text('Press start the game!');

});

$(function () {

    let start = true;

    $("#startbtn").click(function () {

        //request to backserver
        var values = ["Blaine", "Hawkins", "Dutch", "Mac", "Dillon", "Billy", "Poncho"],
            valueToUse = values[Math.floor(Math.random() * values.length)];
        //-----------------------
        start = false;
        $('#data').text(valueToUse);
        $('#subdata').text("Press button!!!");
    });

    $("#blaine").click(function () {

        let str = "blaine"
        const subtitle = "Press start new game.";
        start = true;

        let res = get(str);
        $('#data').text(res.responseText);
        $('#subdata').text(subtitle);
    });

    $("#hawkins").click(function () {
        let str = "hawkins"
        const subtitle = "Press start new game.";
        start = true;
        $('#data').text(get(str).responseText);
        $('#subdata').text(subtitle);
    });

    $("#dutch").click(function () {
        let str = "dutch"
        const subtitle = "Press start new game.";
        start = true;
        $('#data').text(get(str).responseText);
        $('#subdata').text(subtitle);
    });

    $("#mac").click(function () {
        let str = "mac"
        const subtitle = "Press start new game.";
        start = true;
        $('#data').text(get(str).responseText);
        $('#subdata').text(subtitle);
    });

    $("#dillon").click(function () {
        let str = "dillon"
        const subtitle = "Press start new game.";
        start = true;
        $('#data').text(get(str).responseText);
        $('#subdata').text(subtitle);
    });

    $("#billy").click(function () {
        let str = "billy"
        const subtitle = "Press start new game.";
        start = true;
        $('#data').text(get(str).responseText);
        $('#subdata').text(subtitle);
    });
    $("#poncho").click(function () {
        let str = "poncho"
        const subtitle = "Press start new game.";
        start = true;
        $('#data').text(get(str).responseText);
        $('#subdata').text(subtitle);
    });

    function get(str) {
       return  $.ajax({
            type: "GET",
            url: "http://localhost:8080/hunts/enemies/" + str,
            async: false
        });
    }
});

