package com.pattern.strategy.controller;

import com.pattern.strategy.service.HuntFactory;
import com.pattern.strategy.service.PredatorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PredatorController {

    private final PredatorService predator;
    private final HuntFactory huntFactory;

    @GetMapping("/hunts/enemies/{name}")
    public String getHuntingStrategy(@PathVariable("name") String enemyName) {

        predator.setHuntStrategy(huntFactory.findHuntingStrategy(enemyName));

        return predator.doHunt();
    }

    public PredatorController(PredatorService predator, HuntFactory huntFactory) {
        this.predator = predator;
        this.huntFactory = huntFactory;
    }
}
