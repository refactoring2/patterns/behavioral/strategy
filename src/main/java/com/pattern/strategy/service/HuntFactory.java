package com.pattern.strategy.service;

import com.pattern.strategy.service.strategy.hunt.HuntStrategy;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import static com.pattern.strategy.model.Hunt.*;

@Service
public class HuntFactory {

    private final HuntStrategy rangeHuntingStrategy;
    private final HuntStrategy honorHuntingStrategy;
    private final HuntStrategy silentHuntingStrategy;

    public HuntStrategy findHuntingStrategy(String name) {
        if (RANGE.names.contains(name.toLowerCase())) return rangeHuntingStrategy;
        if (SILENT.names.contains(name.toLowerCase())) return silentHuntingStrategy;
        if (HONOR.names.contains(name.toLowerCase())) return honorHuntingStrategy;
        throw new IllegalArgumentException("Hunt type undefined for enemy name: " + name);
    }

    public HuntFactory(@Qualifier("range") HuntStrategy rangeHuntingStrategy, @Qualifier("honor") HuntStrategy honorHuntingStrategy, @Qualifier("silent") HuntStrategy silentHuntingStrategy) {
        this.rangeHuntingStrategy = rangeHuntingStrategy;
        this.honorHuntingStrategy = honorHuntingStrategy;
        this.silentHuntingStrategy = silentHuntingStrategy;
    }
}
