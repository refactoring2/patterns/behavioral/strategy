package com.pattern.strategy.service;

import com.pattern.strategy.service.strategy.hunt.HuntStrategy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class PredatorService {

    private HuntStrategy huntStrategy;

    public String doHunt() {
        return huntStrategy.doHunt();
    }

    public void setHuntStrategy(HuntStrategy huntStrategy) {
        this.huntStrategy = huntStrategy;
    }
}
