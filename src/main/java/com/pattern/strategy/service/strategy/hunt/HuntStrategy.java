package com.pattern.strategy.service.strategy.hunt;

public interface HuntStrategy {

    String doHunt();
}
