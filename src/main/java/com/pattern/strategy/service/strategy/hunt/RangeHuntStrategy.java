package com.pattern.strategy.service.strategy.hunt;

import org.springframework.stereotype.Component;

@Component("range")
public class RangeHuntStrategy implements HuntStrategy{
    @Override
    public String doHunt() {
        return "Range hunt have been called!";
    }
}
