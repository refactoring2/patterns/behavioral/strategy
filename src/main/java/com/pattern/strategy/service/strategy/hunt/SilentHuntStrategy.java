package com.pattern.strategy.service.strategy.hunt;

import org.springframework.stereotype.Component;

@Component("silent")
public class SilentHuntStrategy implements HuntStrategy{
    @Override
    public String doHunt() {
        return "Silent hunt have been called!";
    }
}
