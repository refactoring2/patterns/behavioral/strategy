package com.pattern.strategy.service.strategy.hunt;

import org.springframework.stereotype.Component;

@Component("honor")
public class HonorHuntStrategy implements HuntStrategy {
    @Override
    public String doHunt() {
        return "Honor hunt have been called!";
    }
}
