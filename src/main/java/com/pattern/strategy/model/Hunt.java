package com.pattern.strategy.model;

import java.util.Set;

public enum Hunt {

    RANGE(Set.of("blaine", "dillon", "poncho")),

    SILENT(Set.of("mac", "hawkins")),

    HONOR(Set.of("dutch", "billy"));

    public final Set<String> names;

    Hunt(Set<String> names) {
        this.names = names;
    }
}
